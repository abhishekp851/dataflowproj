package com.mycompany.scala

import org.apache.spark.SparkContext
import org.apache.spark.SparkConf

/**
 * Hello world!
 *;
 */
object App {

  def main(args: Array[String] )=
  {
    println("Hello World!")
    println("This is in text mode!")
    val conf = new SparkConf().setMaster("local").setAppName("TEstApp")
    val sc = new SparkContext(conf)

    System.out.println("SC created " + sc.toString)

    sc.textFile("file://home/vaibhav/simple_archetype_command").persist()
    Thread.sleep(20000);
    System.out.println(sc.hashCode())
  }
}
