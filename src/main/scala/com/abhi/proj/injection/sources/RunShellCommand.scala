package com.abhi.proj.injection.sources

import java.io.BufferedReader
import java.io.InputStreamReader
import java.lang.Runtime._
import org.json.simple._

object RunShellCommand {

    def main(args: Array[String]): Unit = {
      val obj = new RunShellCommand()
      val domainName = "google.com"
      //in mac oxs
      //val command = "ping -c 3 " + domainName

        //val command = "curl http://localhost:50070/webhdfs/v1/tmp2?user.name=hive&op=GETFILESTATUS"

      val command =" curl -i -X PUT http://localhost:50070/webhdfs/v1/tmp/webhdfs/webhdfs-test.txt?user.name=istvan&op=CREATE"

      //in windows
      //String command = "ping -n 3 " + domainName;
      System.out.println("Executing " + command)
      val output = obj.executeCommand(command)
      System.out.println("Executed command " + command)
      System.out.println(output)


    }
}

class RunShellCommand {
    def executeCommand(command: String) = {
      val output = new StringBuffer

      try {
        val p = getRuntime.exec(command)
        p.waitFor()

        val reader = new BufferedReader(new InputStreamReader(p.getInputStream))
        var line :String = ""
        while ({line = reader.readLine; line != null})
          output.append(line + "\n")
      } catch {
        case e: Exception =>
          e.printStackTrace()
      }
      output.toString
    }
}

