package com.abhi.proj.injection.sources

import java.lang.invoke.MethodHandles
import org.apache.log4j.Logger
import scala.io.Source

object FileUtil {

  private val log = Logger.getLogger(MethodHandles.lookup.lookupClass)


  def readFile(file: String): String = {

    log.info("Fetching query from file" + file)


    val stringBuilder = new StringBuilder
    val ls = System.getProperty("line.separator")

    val fileNameAndPath = file
    var fileSource = Source.fromFile(fileNameAndPath)


    try {

      for ( line <- fileSource.getLines()){

        stringBuilder.append(line)
        stringBuilder.append(ls)
      }

      stringBuilder.toString

    } catch {

      case e : Exception => log.error("Exception occurred while fetching query from file" + e.toString)

    } finally {

      fileSource.close()

    }
    log.info("Query fetched from file")
    stringBuilder.mkString
  }
}
