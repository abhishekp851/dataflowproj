package com.abhi.proj.injection.sources

import org.json.simple.JSONObject
import org.json.simple.JSONValue
import org.json.simple.JSONArray


object JsonHandler{
  def main (args: Array[String]): Unit = {

    val output = "{\"jrray\": [{\"Name\":\"Suresh\",\"Location\":\"location1\"},{\"Name\":\"ramesh\",\"Location\":\"location2\"}]}"
    val ob = new JsonHandler()
    ob.jsonParse(output)
  }
}


class JsonHandler {
 def jsonParse(output : String): Unit = {

   val paserval = JSONValue.parse(output).asInstanceOf[JSONObject]
   try {
     for (lis <- output.split("\n")) {
       System.out.println("Element " + lis)
     }
      val jarray = paserval.get("jrray").asInstanceOf[JSONArray]
      val it = jarray.iterator()

      var i =1
        while(it.hasNext) {
          System.out.println("value of i " + i)
          var nextval = it.next().asInstanceOf[JSONObject]
          var name = nextval.get("Name")
          System.out.println("Name " + name )

          if (nextval.get("Name").equals("Suresh"))
            {
              System.out.println("getting entries")
              System.out.println("Location for Suresh is " + nextval.get("Location"))
            }
          i = i+1
     }

     //val permission = paserval.get("FileStatus").asInstanceOf[JSONObject].get("permission")
     //System.out.println("Permission is " + permission)
   } catch {
     case e: Exception => System.out.println("Inside throwable catch")

       /* code for parsing http response

       var location: String = null
       for (lis <- output.split("\n")) {
         System.out.println("Element " + lis)
         if (lis.toString.contains("Name")) {
           location = lis.substring(lis.indexOf(":") + 1)
         }

       } */

   }
 }
}
