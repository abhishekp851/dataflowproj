package com.abhi.proj.process.hive


class HiveDALImpl extends HiveDAL {

    protected var id : Int = 0
    protected var name : String = null
    protected var pass : String = null
    protected var age : Int = 0

    def this(name: String, pass: String, age: Int) {
      this()
      this.name = name
      this.pass = pass
      this.age = age
    }

    def this(id: Int , name: String, pass: String, age: Int) {
      this()
      this.id = id
      this.name = name
      this.pass = pass
      this.age = age
    }

    def getAge: Int = age

    def setAge(age: Int): Unit = {
      this.age = age
    }

    def getId: Int = id

    def setId(id: Int ): Unit = {
      this.id = id
    }

    def getName: String = name

    def setName(name: String): Unit = {
      this.name = name
    }

    def getPass: String = pass

    def setPass(pass: String): Unit = {
      this.pass = pass
    }
}
