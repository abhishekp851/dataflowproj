package com.abhi.proj.process.hive

trait HiveDAL {

  protected var id : Int
  protected var name : String
  protected var pass : String
  protected var age : Int

  def getId: Int
  def getName: String
  def getPass: String
  def getAge: Int

  def setId(id: Int ): Unit
  def setName(name: String ): Unit
  def setPass(pass: String ): Unit
  def setAge(age: Int ): Unit

}
