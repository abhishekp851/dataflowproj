package com.abhi.proj.process.hive

import java.io.{PrintWriter, StringWriter}

import org.apache.hive.jdbc.HiveStatement
import java.sql.SQLException
import java.sql.Statement

import org.apache.log4j.Logger
import java.lang.invoke.MethodHandles

import scala.collection.JavaConverters._


class HiveQueryLogger (stmt : Statement) extends Thread  {

  var log : Logger = Logger.getLogger(MethodHandles.lookup().lookupClass())
  var this.stmt = stmt

  override def run(): Unit= {

    log.info("Started querylogger instance")

     val hiveStatement : HiveStatement= stmt.asInstanceOf[HiveStatement]

      while ( hiveStatement.hasMoreLogs ) {
        try {

          // fetch the log periodically and output to beeline console
          for ( logLine: String <- hiveStatement.getQueryLog(true,0).asScala) {

            log.info (logLine)

          }

          Thread.sleep(1)

        } catch {

          case e: SQLException =>
            val errormsg = "Error while executing hive query"
            log.error(getStackTraceAsString(e))
            return


          case e1 :InterruptedException =>
            Thread.currentThread.interrupt()
            val errormsg= "Getting log thread is interrupted, since query is done!"
            log.error (errormsg)
            new throws[QueryRuntimeException](errormsg)
            return
        }
      }
  }

  def getStackTraceAsString(t: Throwable) = {
    val sw = new StringWriter
    t.printStackTrace(new PrintWriter(sw))
    sw.toString
  }


}
