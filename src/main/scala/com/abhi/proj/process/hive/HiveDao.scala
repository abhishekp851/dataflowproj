package com.abhi.proj.process.hive

import java.sql.Statement

import com.abhi.proj.process.hive._

trait HiveDao {

  def getStmtInstance(url: String, username: String, pass: String): Statement
  def queryExecutor(sql: String, stmt: Statement): Boolean

}
