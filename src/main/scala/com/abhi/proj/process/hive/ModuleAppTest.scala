package com.abhi.proj.process.hive

import org.apache.hadoop.hive.ql.metadata.Hive
import org.apache.log4j.Logger
import java.io._
import java.lang.invoke.MethodHandles
import java.util
import com.abhi.proj.process.hive._
import com.abhi.proj.injection.sources.FileUtil
import scala._
import java.net.URL
import java.net.URLClassLoader
/**
  * App test for hive query executor
  *
  */

object ModuleAppTest {

  val log = Logger.getLogger(MethodHandles.lookup.lookupClass)
  val url: String = "jdbc:hive2://localhost:10000/default"
  val username: String = "hive"
  val pass: String = ""
  val app: HiveDaoImpl = new HiveDaoImpl()

  def main(args: Array[String]) {

    //File:/home/vaibhav/resources/quries pass as arg;

    /**
      * This is test code for printing jars loaded or used while running application
      *
      */

    val cl = ClassLoader.getSystemClassLoader
    val urls = cl.asInstanceOf[URLClassLoader].getURLs
    for (url <- urls) {
      log.info("Loaded " + url.getFile)
    }

    val sql = FileUtil.readFile("/home/vaibhav/resources/quries")
    val stmt = app.getStmtInstance(url, username, pass)
    val qStatus = app.queryExecutor(sql, stmt)

    if (qStatus)
      log.info("Query executed successfully")
    else
      log.warn("Seems like you had bad day.Query did not go through")
  }

  def help(): Unit = {

    log.info("Seems like u have lost your way.Please read below and rerun")
    log.info("Args : QueryFile")
    System.exit(1)

  }

}
