package com.abhi.proj.process.hive

import org.apache.log4j.Logger
import java.lang.invoke.MethodHandles
import java.sql._
import java.util.ArrayList
import java.util.Date

import org.apache.derby.impl.sql.execute.CurrentDatetime

class HiveDaoImpl extends  HiveDao {

  private final val log = Logger.getLogger(MethodHandles.lookup.lookupClass)
    private final val driverName : String = "org.apache.hive.jdbc.HiveDriver"
    private var rs : ResultSet= _
    private var rowCount : Int = 0
    private var con :Connection = _
    private var stmt :Statement = _
    private var hasResultSet :Boolean = false


    def getStmtInstance(url: String, username: String, pass: String): Statement = {

      try
        Class.forName(driverName)
      catch {
        case e: ClassNotFoundException =>
          log.error("Driver class not found")
          e.printStackTrace()
          System.exit(1)
      }

      log.info("Connecting to hive url " + url + " with username as " + username)

      try
        con = DriverManager.getConnection(url, username, pass)
      catch{
        case e : Exception =>
          var errormsg : String = "Exception while getting sql connection"
          throw new Exception(errormsg,e)
      }

      log.info("Connected to hive url")
      log.info("Creating statement for hive execution")

      try
        stmt = con.createStatement
      catch{
        case e : Exception =>
          val errormsg : String = "Exception while creating statement"
          throw new Exception(errormsg,e)
      }

      log.info("Created statement for hive execution")

      stmt
    }

    def queryExecutor(sql: String, stmt: Statement): Boolean = {
      val LogCollector = new HiveQueryLogger(stmt)

      //LogCollector.setDaemon(true)
      LogCollector.setName("Log -Thread")
      LogCollector.start()
      log.info("About to execute query: " + sql)

      try {

         hasResultSet= stmt.execute(sql)
        //System.out.println("Completed" + Date)
      } /*catch {
        case e : SQLException =>
                                val errormsg : String = "Exception occurred while executing statement"
                                log.error(errormsg)
                                new throws[SQLException](errormsg)
      } */finally {
        LogCollector.interrupt()
      }

      if (hasResultSet) {
        log.info("Fetching rows from hive")
        rs = stmt.getResultSet
        val rsmeta = rs.getMetaData
        val colcount = rsmeta.getColumnCount.toInt

        val i : Int = 0

        while ( rs.next()) {
          val row = new ArrayList[Object](colcount)

          for ( i <- 1 to colcount ){
             row.add(rs.getObject(i).toString())
          }

          log.info(row)
          rowCount = rowCount +1
        }

      }
      else {
        log.warn("Query did not return rows")
        return false
      }
      log.info("Rows Fetched : " + rowCount)
      true
    }
}
