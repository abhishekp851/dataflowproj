package com.abhi.proj.process.spark

import org.apache.spark.{SparkConf, SparkContext}

trait SparkContextDefault {

  def getSparkContext(conf:SparkConf):SparkContext

}
