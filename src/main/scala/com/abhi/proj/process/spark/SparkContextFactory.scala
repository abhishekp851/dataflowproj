package com.abhi.proj.process.spark

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.sql.SQLContext

class SparkContextFactory extends SparkContextDefault {

  override def getSparkContext(conf : SparkConf): SparkContext = {

    val sc = new SparkContext(conf)
        sc
  }

  def getSparkContext(): SparkContext = {

    val conf = getDefaultConf()
    val sc = new SparkContext(conf)
    sc
  }

  private def getDefaultConf() : SparkConf = {

    val conf = new SparkConf()

    conf.set("spark.app.name","Spark_app_1")
    conf.set("spark.driver.cores","1")
    conf.set("spark.master","local")
    conf.set("spark.driver.memory","500m")

    conf
  }

  def getSparkSQLContext( sc:SparkContext) : SQLContext = {

    val sqlContext = new SQLContext(sc)

    sqlContext
  }


}
